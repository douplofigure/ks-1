package server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Random;

public class Server {

	public static final String[] cities = {
			"Hamburg",
			"Bremen",
			"Berlin",
			"Leipzig",
			"Köln"
			
	};
	
	ServerSocket serverSocket;
	HashMap<String, String> weatherMap;
	
	/**
	 * Create a new Server instance
	 */
	public Server () throws UnknownHostException, IOException {
	
		serverSocket = new ServerSocket(50000, 0, InetAddress.getLocalHost());
			
		this.weatherMap = new HashMap<String, String>();
		
		Random rand = new Random();
		
		for (int i = 0; i < cities.length; ++i)
			this.weatherMap.put(cities[i], (rand.nextBoolean() ? "Sonnig, " : "Bewölkt, ") + (rand.nextInt(60)+260) + " K");
		
			
		
	}
	
	/**
	 * Used to close the socket.
	 */
	public void finalize() {
		try {
			this.serverSocket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Method used to run the server.
	 * This accepts connections and returns the requested information.
	 * @throws IOException
	 */
	public void run() {
		while (true) {
			Socket client;
			ServerConnection conn = null;
			try {
				client = serverSocket.accept();
				conn = new ServerConnection(client);
				Thread thread = new Thread(conn);
				thread.start();
			} catch (Exception e) {
				
			}
			
		}
		
	}
	
	public String getWeatherInfo(String city) {
		
		return this.weatherMap.get(city);
		
	}
	
	/**
	 * 
	 * class used to Store a connection to the server
	 *
	 */
	private class ServerConnection implements Runnable {
		
		public Socket socket;
		public BufferedReader in;
		public BufferedWriter out;
		
		private boolean keepAlive = true;
		
		/**
		 * Create a new ServerConnection
		 * @param client socket of the client
		 * @throws IOException
		 */
		public ServerConnection(Socket client) throws IOException {
			
			this.socket = client;
			this.in = new BufferedReader(new InputStreamReader(client.getInputStream()));
			this.out = new BufferedWriter(new OutputStreamWriter(client.getOutputStream()));
			
		}
		
		/**
		 * Get a message from client, halts until receiving.
		 * @return a Message send by the client
		 * @throws IOException
		 */
		public String getMessage() throws IOException {
			
			return in.readLine();
			
		}
		
		/**
		 * Send a message to the client
		 * @param msg the message to send
		 * @throws IOException
		 */
		public void sendMessage(String msg) throws IOException {
			
			if (msg.charAt(msg.length()-1) != '\n') {
				
				msg += "\n";
				
			}
			
			out.write(msg);
			out.flush();
			
		}

		@Override
		public void run() {
			
			while (keepAlive) {
				try {
				
				String input = this.getMessage();
				if (input.charAt(0) == '#') break;
				String result = getWeatherInfo(input);
				if (result == null) {
					//conn.sendMessage("Please enter a correct city");
					continue;
				}
				
				if (result.charAt(0) == '#') {
					break;
				}
				
				this.sendMessage("Es ist " + result + " in " + input);
				
				} catch (Exception e) {
					e.printStackTrace(System.err);
					this.keepAlive = false;
				}
				
			}
			System.out.println("Closing connection");
			try {
				this.in.close();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				this.out.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
	}

	
}
