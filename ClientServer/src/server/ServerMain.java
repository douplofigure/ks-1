package server;

import java.io.IOException;
import java.net.UnknownHostException;

public class ServerMain {

	public static void main(String[] args) {
		
		try {
			///create Server and run it.
			Server s = new Server();
			s.run();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
