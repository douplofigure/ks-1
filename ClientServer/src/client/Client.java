package client;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Random;

import server.Server;

public class Client {

	/**
	 * Create a nes Client
	 */
	public Client() {
		
	}
	
	/**
	 * Run the client
	 * sends 40 requests to the server.
	 * @throws IOException
	 */
	public void run() throws IOException {
		
		Random rand = new Random();
		
		ClientConnection conn = new ClientConnection( new Socket(InetAddress.getLocalHost(), 50000));
		for (int i = 0; i < 40; ++i) {
			conn.sendMessage(Server.cities[rand.nextInt(5)]);
			System.out.println(conn.getMessage());
		}
		
		conn.sendMessage("#end_connection");

		
	}
	
	/**
	 * class used to represent a connection from the client.
	 *
	 */
	private class ClientConnection {
		
		///Socket of the connection
		public Socket socket;
		///Reader to get messages from Server
		public BufferedReader in;
		///Writer to send to the server
		public BufferedWriter out;
		
		/**
		 * Create a new Connection
		 * @param socket the socket to connect to
		 * @throws IOException
		 */
		public ClientConnection(Socket socket) throws IOException {
			
			this.socket = socket;
			this.in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			this.out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
			
		}
		
		/**
		 * Closes all readers and writers
		 */
		public void finalize() {
			try {
				this.in.close();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				this.out.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		/**
		 * gets a message from the server
		 * @return the read message
		 * @throws IOException
		 */
		public String getMessage() throws IOException {
			
			return in.readLine();
			
		}
		
		/**
		 * Sends a message to the Server
		 * @param msg the message to send.
		 * @throws IOException
		 */
		public void sendMessage(String msg) throws IOException {
			
			if (msg.charAt(msg.length()-1) != '\n') {
				
				msg += "\n";
				
			}
			
			out.write(msg);
			out.flush();
			
		}
		
	}
	
}
