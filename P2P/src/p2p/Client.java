package p2p;

import java.io.IOException;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import p2p.UDPMessage.MSGType;

public class Client {

	private UDPReciever reciever;
	private List<String> songData;
	private int minPort;
	private int maxPort;
	private boolean keepRunning = true;
	
	public Client(int minPort, int maxPort, String[] data) {
		this.minPort = minPort;
		this.maxPort = maxPort;
		
		this.songData = new ArrayList<>();
		for (int i = 0; i < data.length; ++i)
			this.songData.add(data[i]);
		
		boolean connectOk = false;
		for (int i = minPort; i <= maxPort && !connectOk; ++i) {
			
			try {
				
				this.reciever = new UDPReciever(i);
				connectOk = true;
				
			} catch (SocketException e) {
				
				if (i == maxPort) {
					e.printStackTrace();
					System.exit(1);
				}
				
				connectOk = false;
				
			}
			
		}
		
	}
	
	private void sendReply(UDPMessage msg) {
		this.addAnswerToList(msg.getContent());
		String msgData = getSongListAsString();
		
		UDPMessage outMsg = new UDPMessage(MSGType.ANSWER, msgData, this.reciever.port);
		
		try {
			UDPSender.send(msg.getSenderAddress(), msg.getSenderPort(), outMsg);
		} catch (SocketException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void addAnswerToList(String content) {
		String[] data = content.split("\n");
		for (int i = 0; i < data.length; ++i) {
		
			boolean add = true;
			for (String str : this.songData)
				if (str.compareTo(data[i]) == 0 || data[i] == null || data[i].length() == 0) add = false;
			
			if(add)
				this.songData.add(data[i]);
				
		}
		
		printSongData();
		
	}
	
	private void printSongData() {
		for (String str : this.songData) {
			System.out.println(str);
		}
		
	}

	private void processMessage(UDPMessage msg) {
		switch(msg.getType()) {
		
		case REQUEST:
			sendReply(msg);
			//sendRequest();
			break;
			
		case ANSWER:
			addAnswerToList(msg.getContent());
			break;
			
		default:
			System.err.println("Undefined packet type");
			//sendRequest();
			break;
		
		}
		
	}

	private void sendRequest() {
	
		for (int i = minPort; i <= maxPort; ++i) {
			try {
				UDPMessage msg = new UDPMessage(MSGType.REQUEST, getSongListAsString(), this.reciever.port);
				UDPSender.send(InetAddress.getLocalHost(), i, msg);
			} catch (SocketException e) {
				e.printStackTrace();
			} catch (UnknownHostException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	private String getSongListAsString() {
		String str = "";
		for (String tmp : this.songData) {
			str += tmp + "\n";
		}
		return str;
	}

	public void run() {
		sendRequest();
		while (this.keepRunning ) {
			UDPMessage msg = this.reciever.getMessage();
			if (msg != null) {
				processMessage(msg);
			}
		}
		this.reciever.stop();
	}
	
}
