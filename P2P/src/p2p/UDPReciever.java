package p2p;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class UDPReciever {

	private Queue<UDPMessage> messages;
	public int port;
	private Thread thread;
	private RcThread rcThread;
	private DatagramSocket rcSocket;
	
	public UDPReciever(int port) throws SocketException {
		
		this.messages = new ConcurrentLinkedQueue<UDPMessage>();
	
		this.port = port;
		
		this.rcSocket = new DatagramSocket(port);
		
		
		this.rcThread = new RcThread(this);
		this.thread = new Thread(this.rcThread);
		this.thread.start();
		
		
	}
	
	public void finalize() {
		this.rcSocket.close();
	}
	
	public void stop() {
		this.rcThread.stop();
		this.rcSocket.close();
	}
	
	public UDPMessage getMessage() {
		UDPMessage msg = this.messages.peek();
		if (msg != null) {
			this.messages.remove(msg);
		}
		return msg;
		
	}
	
	private class RcThread implements Runnable {

		private UDPReciever rcv;
		private boolean keepRunning;
		private byte[] readBuffer = new byte[1024];
		
		public RcThread(UDPReciever rcv) {
			this.rcv = rcv;
			this.keepRunning = true;
		}
		
		@Override
		public void run() {
			
			DatagramPacket packet = new DatagramPacket(readBuffer, readBuffer.length);
			
			while (keepRunning) {
				
				try {
					this.rcv.rcSocket.receive(packet);
					
					if (packet.getPort() == this.rcv.port && packet.getAddress().isAnyLocalAddress())
						continue;
					
					UDPMessage msg = UDPMessage.fromByteArray(packet.getData(), packet.getAddress(), packet.getPort());
					this.rcv.messages.add(msg);
					
				} catch (IOException e) {
					e.printStackTrace();
				}
				
			}
			
			
			
		}
		
		public void stop() {
			
			this.keepRunning = false;
			
		}
		
		
	}
	
}
