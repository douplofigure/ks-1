package p2p;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

public class UDPSender {

	
	public static void send(InetAddress dest, int destPort, UDPMessage msg) throws SocketException {
		
		DatagramSocket socket = new DatagramSocket();
		
		byte[] data = msg.toByteArray();
		
		DatagramPacket packet = new DatagramPacket(data, data.length, dest, destPort);
		
		try {
			socket.send(packet);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		socket.close();
		
	}
	
}
